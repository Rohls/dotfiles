# export NNN_PLUG=''

export NNN_FCOLORS='c1e2ac6ae5606cdfc6d6afa7'

export NNN_PLUG='p:preview-tui;'

export NNN_BMS='d:~/Downloads;t:~/.local/share/tor-browser_en-US/Downloads;p:~/Pictures;'
export NNN_FIFO="/tmp/nnn.fifo"

# c1 e2 27 2e 00 60 33 f7 c6 d6 ab c4

# Order 	                    Hex 	Color/Xterm color index

# Block device 	                c1 	    DarkSeaGreen1
# Char device 	                e2 	    Yellow1
# Directory 	                27 	    172
# Executable 	                2e 	    142
# Regular 	                    00 	    229
# Hard link 	                60 	    Plum4
# Symbolic link 	            33 	    108
# Missing OR file details 	    f7 	    223
# Orphaned symbolic link 	    c6 	    DeepPink1
# FIFO 	                        d6 	    Orange1
# Socket 	                    ab 	    175
# Unknown OR 0B regular/exe 	c4 	    167


# original

# c1 e2 27 2e 00 60 33 f7 c6 d6 ab c4

# Block device 	                c1 	    DarkSeaGreen1
# Char device 	                e2 	    Yellow1
# Directory 	                27 	    DeepSkyBlue1
# Executable 	                2e 	    Green1
# Regular 	                    00 	    Normal
# Hard link 	                60 	    Plum4
# Symbolic link 	            33 	    Cyan1
# Missing OR file details 	    f7 	    Grey62
# Orphaned symbolic link 	    c6 	    DeepPink1
# FIFO 	                        d6 	    Orange1
# Socket 	                    ab 	    MediumOrchid1
# Unknown OR 0B regular/exe 	c4 	    Red1

