local opts = {noremap = true}


-- alternate between two most recent buffers
vim.keymap.set('n', '<leader>e', ':e#<cr>', opts)

-- comment out line(s)
vim.keymap.set('x', '<C-s>', '<Plug>Commentary', opts)
vim.keymap.set('n', '<C-s>', '<Plug>CommentaryLine', opts)

vim.keymap.set('n', '<A-z>', function ()
    local line = vim.api.nvim_get_current_line()
    local start = 0
    for i = 1, #line do
        local c = line:sub(i, i)
        if c ~= (' ' or '\t') then
            start = i
            break
        end
    end
    start = start - 1
    local nline = line:sub(0, start) .. 'if (' .. line:sub(start+1) .. ') {}'
    vim.api.nvim_set_current_line(nline)
    vim.api.nvim_input("f{")
    vim.api.nvim_input("a<cr><tab>")
end)

-- indentation
vim.keymap.set('n', '<', ':< <cr>', opts)
vim.keymap.set('n', '>', ':> <cr>', opts)
vim.keymap.set('v', '<', ':< <cr>', opts)
vim.keymap.set('v', '>', ':> <cr>', opts)

-- insert arrow operator (deref)
vim.keymap.set('i', '<C-_>', '->', opts)

vim.keymap.set('n', 'q', ':noh<cr>', opts)
vim.keymap.set('n', '<A-q>', ':noh<cr>', opts)
vim.keymap.set('n', 'Q', 'zA', opts)

vim.keymap.set('n', '<leader>b', ':buffers<cr>', opts)
vim.keymap.set('n', '<leader>q', ':bd<cr>', opts)
vim.keymap.set('n', '<leader>w', ':w<cr>', opts)

-- alternate splits between vertical and horizontal
vim.keymap.set('n', '<leader>j', '<C-w>t<C-w>H<cr>', opts)
vim.keymap.set('n', '<leader>k', '<C-w>t<C-w>K<CR>', opts)

-- gives option to add any of the files in the working directory to buffer
-- vim.keymap.set('n', '<leader>a', ':argadd <c-r>=fnameescape(expand("%:p:h"))<cr>/*<C-d>', opts)

-- insert \n in insert mode
vim.keymap.set('i', '<C-o>', '<Esc>o', opts)


-- -'<Esc>A<CR>{<CR>}<Up><CR>',- insert curly braces
vim.keymap.set('i', '<A-a>', '<Esc>A {<CR><CR>}<Up><Tab>', opts)
-- insert ; at EOL
vim.keymap.set('i', '<A-d>', function()
    local cpos = vim.fn.getpos('.')
    vim.cmd('norm! A;')
    vim.fn.setpos('.', cpos)
end, opts)
vim.keymap.set('i', '<A-s>', '<Esc>A = ', opts)


-- resize splits
vim.keymap.set('n', '<Up>', ':resize +2<cr>', opts)
vim.keymap.set('n', '<Down>', ':resize -2<cr>', opts)
vim.keymap.set('n', '<Left>', ':vertical resize +2<cr>', opts)
vim.keymap.set('n', '<Right>', ':vertical resize -2<cr>', opts)

-- move selected line(s) up and down
vim.keymap.set('v', 'K', ':move \'<-2<CR>gv-gv', opts)
vim.keymap.set('v', 'J', ':move \'>+1<CR>gv-gv', opts)

-- navigate splits
vim.keymap.set('n', '<A-h>', ':wincmd h<cr>', opts)
vim.keymap.set('n', '<A-j>', ':wincmd j<cr>', opts)
vim.keymap.set('n', '<A-k>', ':wincmd k<cr>', opts)
vim.keymap.set('n', '<A-l>', ':wincmd l<cr>', opts)
