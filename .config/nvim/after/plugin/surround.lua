
-------------------------------------------
-- Vim surround
-------------------------------------------
vim.g.surround_no_mappings = 1

-- Surround/delete word with ""
vim.keymap.set('n', 'yt', '<Plug>Ysurroundiw\"')
vim.keymap.set('n', 'yT', '<Plug>Dsurroundiw\"')

-- Surround/delete word with ''
vim.keymap.set('n', 'yr', '<Plug>Ysurroundl\'')
vim.keymap.set('n', 'yR', '<Plug>Dsurround\'')

-- Change surrounding X
vim.keymap.set('n', 'ye', '<Plug>Csurround')

-- Delete Surrounding X
vim.keymap.set('n', 'yw', '<Plug>Dsurround')

-- Surround visual selection with ()
-- vim.keymap.set('v', '<A-a>', '<Plug>Vsurround)')

-- Surround visual selection with ""
-- vim.keymap.set('v', '<A-a>', 'S"')
